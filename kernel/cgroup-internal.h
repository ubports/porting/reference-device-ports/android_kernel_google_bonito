#ifndef CGROUP_INTERNAL_H
#define CGROUP_INTERNAL_H

#include <linux/cgroup.h>

void cgroup_stat_flush(struct cgroup *cgrp);
int cgroup_stat_init(struct cgroup *cgrp);
void cgroup_stat_exit(struct cgroup *cgrp);
void cgroup_stat_boot(void);

#endif
