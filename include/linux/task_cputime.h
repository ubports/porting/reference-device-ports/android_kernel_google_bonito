#ifndef TASK_CPUTIME_H
#define TASK_CPUTIME_H

/**
 * struct task_cputime - collected CPU time counts
 * @utime:              time spent in user mode, in &cputime_t units
 * @stime:              time spent in kernel mode, in &cputime_t units
 * @sum_exec_runtime:   total time spent on the CPU, in nanoseconds
 *
 * This structure groups together three kinds of CPU time that are tracked for
 * threads and thread groups.  Most things considering CPU time want to group
 * these counts together and treat all three of them in parallel.
 */
struct task_cputime { 
	cputime_t utime;
	cputime_t stime; 
	unsigned long long sum_exec_runtime;
};

/**
 * struct prev_cputime - snaphsot of system and user cputime
 * @utime: time spent in user mode
 * @stime: time spent in system mode
 * @lock: protects the above two fields
 *
 * Stores previous user/system time values such that we can guarantee
 * monotonicity.
 */
struct prev_cputime { 
#ifndef CONFIG_VIRT_CPU_ACCOUNTING_NATIVE
	cputime_t utime;
	cputime_t stime;
	raw_spinlock_t lock;
#endif
};

#endif
