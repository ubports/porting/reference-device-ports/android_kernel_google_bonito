#ifndef CPUTIME_H
#define CPUTIME_H

extern void cputime_adjust(struct task_cputime *curr, struct prev_cputime *prev,
			   u64 *ut, u64 *st);

#endif
